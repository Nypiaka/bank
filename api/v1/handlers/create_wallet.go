package handlers

import (
	"bank/internal/storage"
	"encoding/json"
	"errors"
	"log"
	"net/http"
)

func CreateWallet(dao storage.DaoInterface) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		requestId := r.Header.Get("X-Request-Id")
		logPrefix := "[" + requestId + "] "
		log.SetPrefix(logPrefix)
		log.Println("Request to create a wallet")
		acc, err := dao.CreateAccount()
		if err != nil {
			if errors.Is(err, storage.ErrAccountNotFound) {
				log.Println("Error creating account: ", err)
				w.WriteHeader(http.StatusNotFound)
				return
			}
			log.Println("Internal server error: ", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = json.NewEncoder(w).Encode(acc)
		if err != nil {
			log.Println("Internal server error: ", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		log.Println("Account created successfully")
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
	}
}
