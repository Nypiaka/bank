package handlers

import (
	"bank/internal/storage"
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func GetTransactions(dao storage.DaoInterface) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		requestId := r.Header.Get("X-Request-Id")
		logPrefix := "[" + requestId + "] "
		log.SetPrefix(logPrefix)
		log.Println("Request to get history of transactions")
		vars := mux.Vars(r)
		id := vars["walletId"]
		ops, err := dao.GetAccountHistory(id)
		if err != nil {
			log.Println("Error getting account history: ", err)
			if errors.Is(err, storage.ErrAccountNotFound) {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = json.NewEncoder(w).Encode(ops)
		if err != nil {
			log.Println("Error getting account history: ", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		log.Println("History retrieved successfully")
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
	}
}
