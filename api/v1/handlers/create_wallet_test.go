package handlers

import (
	"bank/api/v1/handlers/mock"
	"bank/domain"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCreateWallet(t *testing.T) {
	daoMock := mock.NewDaoInterfaceMock(t)
	handler := CreateWallet(daoMock)
	daoMock.CreateAccountMock.Return(&domain.Account{
		Id:     "random-id",
		Amount: 100,
	}, nil)
	req, err := http.NewRequest(http.MethodPost, "/api/v1/wallet", nil)
	require.NoError(t, err)

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	require.Equal(t, rr.Code, http.StatusOK)

	body := rr.Body.String()

	require.Equal(t, "{\"id\":\"random-id\",\"balance\":100}\n", body)
}
