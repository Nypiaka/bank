package handlers

import (
	"bank/domain"
	"bank/internal/storage"
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func TransferAmount(dao storage.DaoInterface) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		requestId := r.Header.Get("X-Request-Id")
		logPrefix := "[" + requestId + "] "
		log.SetPrefix(logPrefix)
		log.Println("Request to transfer amount")
		vars := mux.Vars(r)
		id := vars["walletId"]
		var tp domain.TransactionPart
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&tp); err != nil {
			log.Println("Error decoding body: ", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		defer r.Body.Close()
		err := dao.MakeTransfer(id, tp.To, tp.Amount)
		if err == nil {
			log.Println("Transfer made successfully")
			w.WriteHeader(http.StatusOK)
			return
		} else if errors.Is(err, storage.ErrFromAccountNotFound) {
			log.Println("From account not found: ", err)
			w.WriteHeader(http.StatusNotFound)
			return
		} else if errors.Is(err, storage.ErrWrongAmount) || errors.Is(err, storage.ErrToAccountNotFound) {
			log.Println("Wrong request data: ", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}
}
