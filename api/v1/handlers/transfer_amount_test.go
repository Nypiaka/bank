package handlers

import (
	"bank/api/v1/handlers/mock"
	"bank/internal/storage"
	"bytes"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

const toAccount = "to-wallet-id"

func transferAmount(t *testing.T, expErr error, expCode int) {
	requestPath := "/api/v1/wallet/" + walletId + "/send"

	daoMock := mock.NewDaoInterfaceMock(t)
	daoMock.MakeTransferMock.When(walletId, toAccount, 100.0).Then(expErr)

	input := fmt.Sprintf(`{"to": "%s", "amount": %f}`, toAccount, 100.0)

	req, err := http.NewRequest(http.MethodPost, requestPath, bytes.NewReader([]byte(input)))
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}/send", TransferAmount(daoMock)).Methods(http.MethodPost)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, expCode, rr.Code)

	require.NoError(t, err)
}

func TestTransferAmount(t *testing.T) {
	transferAmount(t, nil, http.StatusOK)
}

func TestNoToAccountTransfer(t *testing.T) {
	transferAmount(t, storage.ErrFromAccountNotFound, http.StatusNotFound)
}

func TestNoFromAccountTransfer(t *testing.T) {
	transferAmount(t, storage.ErrToAccountNotFound, http.StatusBadRequest)
}

func TestWrongAmountAccountTransfer(t *testing.T) {
	transferAmount(t, storage.ErrWrongAmount, http.StatusBadRequest)
}
