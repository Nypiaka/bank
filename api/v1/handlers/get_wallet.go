package handlers

import (
	"bank/internal/storage"
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func GetWallet(dao storage.DaoInterface) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		requestId := r.Header.Get("X-Request-Id")
		logPrefix := "[" + requestId + "] "
		log.SetPrefix(logPrefix)
		log.Println("Request to get a wallet")
		vars := mux.Vars(r)
		id := vars["walletId"]
		acc, err := dao.GetAccount(id)
		if err != nil {
			log.Println("Error getting account: ", err)
			if errors.Is(err, storage.ErrAccountNotFound) {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = json.NewEncoder(w).Encode(acc)
		if err != nil {
			log.Println("Error getting account: ", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		log.Println("Account retrieved successfully")
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
	}
}
