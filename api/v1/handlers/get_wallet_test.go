package handlers

import (
	"bank/api/v1/handlers/mock"
	"bank/domain"
	"bank/internal/storage"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetWallet(t *testing.T) {
	requestPath := "/api/v1/wallet/" + walletId

	daoMock := mock.NewDaoInterfaceMock(t)
	daoMock.GetAccountMock.When(walletId).Then(&domain.Account{
		Id:     "id1",
		Amount: 100,
	}, nil)

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}", GetWallet(daoMock)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)

	require.NoError(t, err)

	require.Equal(t, "{\"id\":\"id1\",\"balance\":100}\n", rr.Body.String())
}

func TestGetNoWallet(t *testing.T) {
	requestPath := "/api/v1/wallet/" + walletId

	daoMock := mock.NewDaoInterfaceMock(t)
	daoMock.GetAccountMock.When(walletId).Then(nil, storage.ErrAccountNotFound)

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}", GetWallet(daoMock)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusNotFound, rr.Code)
}
