package handlers

import (
	"bank/api/v1/handlers/mock"
	"bank/domain"
	"bank/internal/storage"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

const walletId = "test-wallet-id"

func getTransactions(t *testing.T, history []domain.Transaction) {
	requestPath := "/api/v1/wallet/" + walletId + "/history"

	daoMock := mock.NewDaoInterfaceMock(t)
	expectedTransactions := history
	daoMock.GetAccountHistoryMock.When(walletId).Then(expectedTransactions, nil)

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}/history", GetTransactions(daoMock)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)

	var responseTransactions []domain.Transaction
	err = json.NewDecoder(rr.Body).Decode(&responseTransactions)
	require.NoError(t, err)

	require.Equal(t, expectedTransactions, responseTransactions)
}

func TestSimpleGetTransactions(t *testing.T) {
	getTransactions(t,
		[]domain.Transaction{
			{Amount: 100, From: "sender-id", To: walletId, Time: "some-time"},
		},
	)
}

func TestEmptyGetTransactions(t *testing.T) {
	getTransactions(t,
		[]domain.Transaction{})
}

func TestNoAccountGetTransactions(t *testing.T) {
	requestPath := "/api/v1/wallet/" + walletId + "/history"

	daoMock := mock.NewDaoInterfaceMock(t)
	daoMock.GetAccountHistoryMock.When(walletId).Then(nil, storage.ErrAccountNotFound)

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}/history", GetTransactions(daoMock)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusNotFound, rr.Code)

}
