package domain

type TransactionPart struct {
	To     string  `json:"to"`
	Amount float64 `json:"amount"`
}
