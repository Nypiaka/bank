package domain

type Account struct {
	Id     string  `json:"id"`
	Amount float64 `json:"balance"`
}
