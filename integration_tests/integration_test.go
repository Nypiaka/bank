package integration_tests

import (
	"bank/api/v1/handlers"
	"bank/domain"
	"bank/internal/storage"
	"bank/internal/storage/sqlite"
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"os"
	"regexp"
	"testing"
)

const (
	bank_storage_path         = "./storage/bank.db"
	transactions_storage_path = "./storage/transactions.db"
	rfc3339_format            = `^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(\.\d+)?(Z|([+-]\d{2}:\d{2}))$`
)

func createDao() *sqlite.Dao {
	_ = os.Mkdir("storage", os.ModePerm)
	dao, _ := sqlite.NewDao(bank_storage_path, transactions_storage_path)
	dao.Init()
	return dao
}

func removeDao() {
	_ = os.RemoveAll("storage/")
}

func getBank() *sql.DB {
	bank, _ := sql.Open("sqlite3", bank_storage_path)
	return bank
}

func getTransactions() *sql.DB {
	transactions, _ := sql.Open("sqlite3", transactions_storage_path)
	return transactions
}

func getAccount(bank *sql.DB, id string) (*domain.Account, error) {
	fun := "storage.sqlite.getAccount"
	bankStmt, err := bank.Prepare("SELECT * FROM bank WHERE id = ?")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	found, err := bankStmt.Query(id)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	defer found.Close()
	if !found.Next() {
		return nil, storage.ErrAccountNotFound
	}
	var resId string
	var amount float64
	err = found.Scan(&resId, &amount)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	return &domain.Account{
			Id:     id,
			Amount: amount,
		},
		nil
}

func getHistory(bank, transactions *sql.DB, id string) ([]domain.Transaction, error) {
	_, err := getAccount(bank, id)
	if err != nil {
		return nil, err
	}
	stmt, err := transactions.Prepare("SELECT * FROM transactions WHERE accFrom = ? OR accTo = ?")
	if err != nil {
		return nil, err
	}
	res, err := stmt.Query(id, id)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	var hist []domain.Transaction
	for res.Next() {
		var from string
		var to string
		var amount float64
		var time string
		iterErr := res.Scan(&from, &to, &amount, &time)
		if iterErr == nil {
			transaction := domain.Transaction{
				From:   from,
				To:     to,
				Amount: amount,
				Time:   time,
			}
			hist = append(hist, transaction)
		}
	}
	return hist, nil
}

func createAccount(bank *sql.DB, dao *sqlite.Dao) *domain.Account {
	handler := handlers.CreateWallet(dao)

	req, _ := http.NewRequest(http.MethodPost, "/api/v1/wallet", nil)

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)
	body := rr.Body.String()

	var data map[string]interface{}
	_ = json.Unmarshal([]byte(body), &data)
	id, _ := data["id"].(string)
	acc, _ := getAccount(bank, id)
	return acc
}

func transfer(from, to string, amount float64, dao *sqlite.Dao) (*httptest.ResponseRecorder, error) {
	requestPath := "/api/v1/wallet/" + from + "/send"

	input := fmt.Sprintf(`{"to": "%s", "amount": %f}`, to, amount)

	req, err := http.NewRequest(http.MethodPost, requestPath, bytes.NewReader([]byte(input)))

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}/send", handlers.TransferAmount(dao)).Methods(http.MethodPost)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	return rr, err
}

func TestCreateAccounts(t *testing.T) {
	dao := createDao()
	bank := getBank()
	handler := handlers.CreateWallet(dao)
	for a := 0; a < 100; a++ {
		req, err := http.NewRequest(http.MethodPost, "/api/v1/wallet", nil)
		require.NoError(t, err)

		rr := httptest.NewRecorder()
		handler.ServeHTTP(rr, req)

		require.Equal(t, rr.Code, http.StatusOK)

		body := rr.Body.String()

		var data map[string]interface{}
		err = json.Unmarshal([]byte(body), &data)
		require.NoError(t, err)

		id, ok := data["id"].(string)
		require.True(t, ok)

		balance, ok := data["balance"].(float64)
		require.True(t, ok)

		acc, err := getAccount(bank, id)
		require.NoError(t, err)
		require.Equal(t, id, acc.Id)
		require.Equal(t, balance, acc.Amount)
	}
	removeDao()
}

func TestSimpleTransfer(t *testing.T) {
	dao := createDao()
	bank := getBank()
	trans := getTransactions()

	from := createAccount(bank, dao)
	to := createAccount(bank, dao)

	rr, err := transfer(from.Id, to.Id, 10.0, dao)

	require.Equal(t, http.StatusOK, rr.Code)

	require.NoError(t, err)

	fromNow, err := getAccount(bank, from.Id)
	require.NoError(t, err)
	toNow, err := getAccount(bank, to.Id)
	require.NoError(t, err)
	require.Equal(t, 90.0, fromNow.Amount)
	require.Equal(t, 110.0, toNow.Amount)

	fromHist, err := getHistory(bank, trans, fromNow.Id)
	require.NoError(t, err)
	toHist, err := getHistory(bank, trans, toNow.Id)
	require.NoError(t, err)

	require.Equal(t, len(fromHist), 1)
	require.Equal(t, fromHist[0].From, fromNow.Id)
	require.Equal(t, fromHist[0].To, toNow.Id)
	ok, err := regexp.MatchString(rfc3339_format, fromHist[0].Time)
	require.NoError(t, err)
	require.True(t, ok)

	require.Equal(t, len(toHist), 1)
	require.Equal(t, toHist[0].From, fromNow.Id)
	require.Equal(t, toHist[0].To, toNow.Id)
	ok, err = regexp.MatchString(rfc3339_format, toHist[0].Time)
	require.NoError(t, err)
	require.True(t, ok)
	removeDao()
}

func TestTransferWithWrongAmount(t *testing.T) {
	dao := createDao()
	bank := getBank()
	trans := getTransactions()

	from := createAccount(bank, dao)
	to := createAccount(bank, dao)

	rr, err := transfer(from.Id, to.Id, 1000.0, dao)

	require.Equal(t, http.StatusBadRequest, rr.Code)

	require.NoError(t, err)

	fromNow, err := getAccount(bank, from.Id)
	require.NoError(t, err)
	toNow, err := getAccount(bank, to.Id)
	require.NoError(t, err)
	require.Equal(t, 100.0, fromNow.Amount)
	require.Equal(t, 100.0, toNow.Amount)

	fromHist, err := getHistory(bank, trans, fromNow.Id)
	require.NoError(t, err)
	toHist, err := getHistory(bank, trans, toNow.Id)
	require.NoError(t, err)

	require.Equal(t, len(fromHist), 0)

	require.Equal(t, len(toHist), 0)

	removeDao()
}

func TestTransferWithNoFromAccount(t *testing.T) {
	dao := createDao()
	bank := getBank()
	trans := getTransactions()

	to := createAccount(bank, dao)

	rr, err := transfer("random", to.Id, 100.0, dao)

	require.Equal(t, http.StatusNotFound, rr.Code)

	require.NoError(t, err)

	toNow, err := getAccount(bank, to.Id)
	require.NoError(t, err)
	require.Equal(t, 100.0, toNow.Amount)

	toHist, err := getHistory(bank, trans, toNow.Id)
	require.NoError(t, err)

	require.Equal(t, len(toHist), 0)

	removeDao()
}

func TestTransferWithNoToAccount(t *testing.T) {
	dao := createDao()
	bank := getBank()
	trans := getTransactions()

	from := createAccount(bank, dao)

	rr, err := transfer(from.Id, "random", 100.0, dao)

	require.Equal(t, http.StatusBadRequest, rr.Code)

	require.NoError(t, err)

	fromNow, err := getAccount(bank, from.Id)
	require.NoError(t, err)
	require.Equal(t, 100.0, fromNow.Amount)

	fromHist, err := getHistory(bank, trans, fromNow.Id)
	require.NoError(t, err)

	require.Equal(t, len(fromHist), 0)

	removeDao()
}

func TestGetHistorySimple(t *testing.T) {
	dao := createDao()
	bank := getBank()
	from := createAccount(bank, dao)
	to := createAccount(bank, dao)

	_, _ = transfer(from.Id, to.Id, 1.0, dao)
	_, _ = transfer(from.Id, to.Id, 2.0, dao)
	_, _ = transfer(from.Id, to.Id, 3.0, dao)
	_, _ = transfer(from.Id, to.Id, 4.0, dao)

	requestPath := "/api/v1/wallet/" + from.Id + "/history"

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}/history", handlers.GetTransactions(dao)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)

	var responseTransactions []domain.Transaction
	err = json.NewDecoder(rr.Body).Decode(&responseTransactions)
	require.NoError(t, err)

	require.Equal(t, 4, len(responseTransactions))
	k := 0
	for i := 1.0; i < 5.0; i++ {
		require.Equal(t, i, responseTransactions[k].Amount)
		require.Equal(t, from.Id, responseTransactions[k].From)
		require.Equal(t, to.Id, responseTransactions[k].To)
		ok, err := regexp.MatchString(rfc3339_format, responseTransactions[k].Time)
		require.NoError(t, err)
		require.True(t, ok)
		k++
	}

	removeDao()
}

func TestGetHistoryNoAccount(t *testing.T) {
	dao := createDao()
	requestPath := "/api/v1/wallet/" + "random" + "/history"

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}/history", handlers.GetTransactions(dao)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusNotFound, rr.Code)
	removeDao()
}

func TestGetHistoryEmpty(t *testing.T) {
	dao := createDao()
	bank := getBank()
	from := createAccount(bank, dao)

	requestPath := "/api/v1/wallet/" + from.Id + "/history"

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}/history", handlers.GetTransactions(dao)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)

	var responseTransactions []domain.Transaction
	err = json.NewDecoder(rr.Body).Decode(&responseTransactions)
	require.NoError(t, err)

	require.Equal(t, 0, len(responseTransactions))

	removeDao()
}

func TestGetAccountSimple(t *testing.T) {
	dao := createDao()
	bank := getBank()
	acc := createAccount(bank, dao)

	requestPath := "/api/v1/wallet/" + acc.Id

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}", handlers.GetWallet(dao)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusOK, rr.Code)

	require.NoError(t, err)

	body := rr.Body.String()

	var data map[string]interface{}
	err = json.Unmarshal([]byte(body), &data)
	require.NoError(t, err)

	id, ok := data["id"].(string)
	require.True(t, ok)

	balance, ok := data["balance"].(float64)
	require.True(t, ok)

	require.NoError(t, err)
	require.Equal(t, id, acc.Id)
	require.Equal(t, balance, acc.Amount)

	removeDao()
}

func TestGetNoAccount(t *testing.T) {
	dao := createDao()

	requestPath := "/api/v1/wallet/" + "random"

	req, err := http.NewRequest(http.MethodGet, requestPath, nil)
	require.NoError(t, err)

	router := mux.NewRouter()
	router.Handle("/api/v1/wallet/{walletId}", handlers.GetWallet(dao)).Methods(http.MethodGet)

	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	require.Equal(t, http.StatusNotFound, rr.Code)

	removeDao()
}
