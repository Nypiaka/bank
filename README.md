# EWallet

## Запуск

Чтобы запустить приложение необходимо в корне репозитория написать 
```bash
./run.sh
```
Для остановки требуется написать
```bash
./stop.sh
```
соответственно
Сервер работает на стандартном порте 8080

## Пример работы с сервером

Создание кошелька 
```bash
curl http://localhost:8080/api/v1/wallet --request "POST"
```
Результат
```json
{"id":"c0093af9-48aa-4d4e-8342-6202d264f554","balance":100}
```
Перевод средств с одного кошелька на другой
```bash
curl http://localhost:8080/api/v1/wallet/713c8c0f-e3e2-41bc-b68c-cf555e3bb786/send --include --header "Content-Type: application/json" --request "POST" --data '{"to": "c0093af9-48aa-4d4e-8342-6202d264f554","amount": 1}'
```
Результат
```bash
HTTP/1.1 200 OK
Date: Sat, 03 Feb 2024 14:21:53 GMT
Content-Length: 0
```
Получение историй входящих и исходящих транзакций
```bash
curl http://localhost:8080/api/v1/wallet/713c8c0f-e3e2-41bc-b68c-cf555e3bb786/history --include --header "Content-Type: application/json" --request "GET"
```
Результат
```json
[{"from":"713c8c0f-e3e2-41bc-b68c-cf555e3bb786","to":"c0093af9-48aa-4d4e-8342-6202d264f554","amount":1,"time":"2024-02-03T19:21:36+05:00"},{"from":"713c8c0f-e3e2-41bc-b68c-cf555e3bb786","to":"c0093af9-48aa-4d4e-8342-6202d264f554","amount":1,"time":"2024-02-03T19:21:52+05:00"},{"from":"713c8c0f-e3e2-41bc-b68c-cf555e3bb786","to":"c0093af9-48aa-4d4e-8342-6202d264f554","amount":1,"time":"2024-02-03T19:21:53+05:00"}]
```
Получение текущего состояния кошелька
```bash
curl http://localhost:8080/api/v1/wallet/713c8c0f-e3e2-41bc-b68c-cf555e3bb786 --request "GET"
```
Результат
```json
{"id":"713c8c0f-e3e2-41bc-b68c-cf555e3bb786","balance":97}
```

## Тестирование
Сервер покрыт интеграционными тестами. Также для каждой ручки написаны юнит-тесты с моканной базой данных.

## Докер
Для сборки достаточно
```bash
sudo docker build --tag bank .
```
Запуск происходит через
```bash
sudo docker run --name=bank -p 80:8080 bank
```
Где вместо 80 можно поставить другой порт

## CI
Для репозитория написан yml-файл, запускающий линтер и тесты при любом коммите на gitlab.

## Конфиг
В конфиге лежат пути к создаваемым базам данных. Можно настроить по своему усмотрению.
