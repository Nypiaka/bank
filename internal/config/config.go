package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"os"
)

type Config struct {
	BankStoragePath         string `yaml:"bank_storage_path" env-required:"true"`
	TransactionsStoragePath string `yaml:"transactions_storage_path" env-required:"true"`
}

func MustLoad() *Config {

	curDir, err := os.Getwd()
	var configPath string
	if err == nil {
		configPath = curDir + "/config/local.yaml"
	}

	var cfg Config

	_ = cleanenv.ReadConfig(configPath, &cfg)

	return &cfg
}
