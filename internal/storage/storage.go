package storage

import "errors"

var (
	ErrAccountNotFound     = errors.New("account not found")
	ErrWrongAmount         = errors.New("wrong amount")
	ErrFromAccountNotFound = errors.New("from account not found")
	ErrToAccountNotFound   = errors.New("to account not found")
)
