package sqlite

import (
	"bank/domain"
	"bank/internal/storage"
	"database/sql"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
)

type Storage struct {
	db *sql.DB
	mu sync.Mutex
}

type Dao struct {
	bank         *Storage
	transactions *Storage
}

func NewDao(bankPath, transactionsPath string) (*Dao, error) {
	bankDb, bankErr := createDb(`CREATE TABLE IF NOT EXISTS bank (
		id TEXT PRIMARY KEY UNIQUE, 
		amount REAL NOT NULL);`, bankPath)
	if bankErr != nil {
		return nil, fmt.Errorf("%s: %w", "bank storage.sqlite.New", bankErr)
	}
	transactionsDb, transactionsErr := createDb(`CREATE TABLE IF NOT EXISTS transactions (
		accFrom TEXT NOT NULL, 
		accTo TEXT NOT NULL, 
		amount REAL NOT NULL,
		transferredAt TEXT NOT NULL);`, transactionsPath)
	if transactionsErr != nil {
		return nil, fmt.Errorf("%s: %w", "transactions storage.sqlite.New", transactionsErr)
	}
	return &Dao{
		bank:         &Storage{db: bankDb, mu: sync.Mutex{}},
		transactions: &Storage{db: transactionsDb, mu: sync.Mutex{}}}, nil
}

func (d *Dao) Init() {
	_ = d.bank
	_ = d.transactions
}

func createDb(onCreate, path string) (*sql.DB, error) {
	if _, err := os.Stat(filepath.Dir(path)); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir(filepath.Dir(path), os.ModePerm)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", "storage.sqlite.createDb", err)
		}
	}
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", "storage.sqlite.createDb", err)
	}
	stmt, err := db.Prepare(onCreate)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", "storage.sqlite.createDb", err)
	}
	_, err = stmt.Exec()
	if err != nil {
		return nil, fmt.Errorf("%s: %w", "storage.sqlite.createDb", err)
	}
	return db, nil
}

func (d *Dao) CreateAccount() (*domain.Account, error) {
	d.bank.mu.Lock()
	defer d.bank.mu.Unlock()
	fun := "storage.sqlite.CreateAccount"
	id := uuid.New().String()
	stmt, err := d.bank.db.Prepare("INSERT INTO bank(id, amount) VALUES (?, ?)")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	_, err = stmt.Exec(id, "100.0")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	return &domain.Account{
		Id:     id,
		Amount: 100.0,
	}, nil
}

func (d *Dao) MakeTransfer(from, to string, sum float64) error {
	d.bank.mu.Lock()
	d.transactions.mu.Lock()
	defer d.bank.mu.Unlock()
	defer d.transactions.mu.Unlock()
	fun := "storage.sqlite.MakeTransfer"
	fromAcc, err := getAccount(d.bank, from)
	if errors.Is(err, storage.ErrAccountNotFound) {
		return storage.ErrFromAccountNotFound
	} else if err != nil {
		return fmt.Errorf("%s: %w", fun, err)
	}
	toAcc, err := getAccount(d.bank, to)
	if errors.Is(err, storage.ErrAccountNotFound) {
		return storage.ErrToAccountNotFound
	} else if err != nil {
		return fmt.Errorf("%s: %w", fun, err)
	}
	if fromAcc.Amount < sum || sum < 0 {
		return storage.ErrWrongAmount
	}
	fromAcc.Amount -= sum
	toAcc.Amount += sum
	err = updateAccount(fromAcc, d.bank)
	if err != nil {
		return fmt.Errorf("%s: %w", fun, err)
	}
	err = updateAccount(toAcc, d.bank)
	if err != nil {
		return fmt.Errorf("%s: %w", fun, err)
	}
	err = createLog(from, to, sum, d.transactions)
	if err != nil {
		return fmt.Errorf("%s: %w", fun, err)
	}
	return nil
}

func (d *Dao) GetAccount(id string) (*domain.Account, error) {
	d.bank.mu.Lock()
	defer d.bank.mu.Unlock()
	acc, err := getAccount(d.bank, id)
	if err != nil {
		return nil, err
	}
	return acc, nil
}

func (d *Dao) GetAccountHistory(id string) ([]domain.Transaction, error) {
	d.bank.mu.Lock()
	d.transactions.mu.Lock()
	defer d.bank.mu.Unlock()
	defer d.transactions.mu.Unlock()
	fun := "storage.sqlite.GetAccountHistory"
	_, err := getAccount(d.bank, id)
	if err != nil {
		return nil, err
	}
	stmt, err := d.transactions.db.Prepare("SELECT * FROM transactions WHERE accFrom = ? OR accTo = ?")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	res, err := stmt.Query(id, id)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	defer res.Close()
	var hist []domain.Transaction
	for res.Next() {
		var from string
		var to string
		var amount float64
		var time string
		iterErr := res.Scan(&from, &to, &amount, &time)
		if iterErr == nil {
			transaction := domain.Transaction{
				From:   from,
				To:     to,
				Amount: amount,
				Time:   time,
			}
			hist = append(hist, transaction)
		}
	}
	return hist, nil
}

func getAccount(bank *Storage, id string) (*domain.Account, error) {
	fun := "storage.sqlite.getAccount"
	bankStmt, err := bank.db.Prepare("SELECT * FROM bank WHERE id = ?")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	found, err := bankStmt.Query(id)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	defer found.Close()
	if !found.Next() {
		return nil, storage.ErrAccountNotFound
	}
	var resId string
	var amount float64
	err = found.Scan(&resId, &amount)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fun, err)
	}
	return &domain.Account{
			Id:     id,
			Amount: amount,
		},
		nil
}

func updateAccount(acc *domain.Account, bank *Storage) error {
	stmt, err := bank.db.Prepare("UPDATE bank SET amount = ? WHERE id = ?")
	if err != nil {
		return fmt.Errorf("%s: %w", "storage.sqlite.updateAccount", err)
	}
	_, err = stmt.Exec(acc.Amount, acc.Id)
	if err != nil {
		return fmt.Errorf("%s: %w", "storage.sqlite.updateAccount", err)
	}
	return nil
}

func createLog(from, to string, sum float64, transactions *Storage) error {
	t := time.Now()
	rfc3339time := t.Format(time.RFC3339)
	stmt, err := transactions.db.Prepare("INSERT INTO transactions(accFrom, accTo, amount, transferredAt) VALUES (?, ?, ?, ?)")
	if err != nil {
		return fmt.Errorf("%s: %w", "storage.sqlite.createLog", err)
	}
	_, err = stmt.Exec(from, to, sum, rfc3339time)
	if err != nil {
		return fmt.Errorf("%s: %w", "storage.sqlite.createLog", err)
	}
	return nil
}
