package storage

import (
	"bank/domain"
	_ "github.com/gojuno/minimock"
)

type DaoInterface interface {
	CreateAccount() (*domain.Account, error)
	MakeTransfer(from, to string, sum float64) error
	GetAccount(id string) (*domain.Account, error)
	GetAccountHistory(id string) ([]domain.Transaction, error)
	Init()
}
