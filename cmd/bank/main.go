package main

import (
	"bank/api/v1/handlers"
	"bank/internal/config"
	"bank/internal/storage/sqlite"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	cfg := config.MustLoad()
	dao, err :=
		sqlite.NewDao(cfg.BankStoragePath, cfg.TransactionsStoragePath)
	if err != nil {
		log.Fatal("Error initializing storage:", err)
	}
	dao.Init()

	router := mux.NewRouter()
	router.HandleFunc("/api/v1/wallet", handlers.CreateWallet(dao)).Methods("POST")
	router.HandleFunc("/api/v1/wallet/{walletId}/send", handlers.TransferAmount(dao)).Methods("POST")
	router.HandleFunc("/api/v1/wallet/{walletId}", handlers.GetWallet(dao)).Methods("GET")
	router.HandleFunc("/api/v1/wallet/{walletId}/history", handlers.GetTransactions(dao)).Methods("GET")

	port := ":8080"
	log.Printf("Server is starting on port %s...\n", port)
	err = http.ListenAndServe(port, router)
	if err != nil {
		log.Fatal("Error starting server:", err)
	}
}
