PID=$(lsof -t -i:8080)

if [ -z "$PID" ]; then
  echo "No 8080 server found"
else
  echo "Server on port 8080 stoping (PID: $PID)"
  kill $PID
  echo "Server stoped"
fi
