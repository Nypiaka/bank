FROM golang:latest

WORKDIR /app

COPY go.mod go.sum ./

COPY . .

RUN go build -o bank-app ./cmd/bank/main.go

EXPOSE 8080

CMD ["./bank-app"]
